import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { environment } from './../../environments/environment';

// Imports para poder consumir API REST

import { HttpClient, HttpHeaders, HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ApiService {

  private API_URL: string;

  constructor(
    private http: HttpClient
  ) {
      this.API_URL = environment.apiUrl + '/rates';
   }

  public getAll(): Observable<any> {
    return this.http.get(this.API_URL);
  }
  public getbyId(id: string): Observable<any> {
    return this.http.get(this.API_URL + '/' + id);
  }
  public add(item: string): Observable<any> {
    const body = JSON.stringify(item);
    return this.http.post(this.API_URL, body);
  }
  public update(id: string, itemToUpdate: any): Observable<any> {
    return this.http.put(this.API_URL + '/' + id, JSON.stringify(itemToUpdate));
  }
  public delete(id: number): Observable<any> {
    return this.http.delete(this.API_URL + '/' + id);
  }
}
@Injectable()
export class CustomInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.headers.has('Content-Type')) {
            req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        }

        req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        console.log(JSON.stringify(req.headers));
        return next.handle(req);
    }
}
