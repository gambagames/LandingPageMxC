import { Component, OnInit } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  columnNum = 0;
  columnNum1 = 0;

  constructor(media: ObservableMedia) {
    media.asObservable()
    .subscribe((change: MediaChange) => {
      // alert(change.mqAlias);
      if (change.mqAlias === 'xs') {
        this.columnNum = 1;
      } else if (change.mqAlias === 'sm') {
        this.columnNum = 1;
        this.columnNum1 = 0;
        } else {
        this.columnNum = 4;
        this.columnNum1 = 1;
      }
    });
   }

  ngOnInit() {
  }

}
