import { Component, OnInit} from '@angular/core';

import { ApiService } from './../services/api.service';
import { PipeResolver } from '@angular/compiler';
import { ObservableMedia, MediaChange } from '@angular/flex-layout';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})

export class InicioComponent implements OnInit {

  public message: string;
  public foreign_exchange: any[];
  public temporalid: string;
  public rates: any[];
  public data1 = 0.0000;
  public data2: number;
  public result: number;
  columnNum = 0;

  constructor(private _dataApiService: ApiService, media: ObservableMedia) {

      media.asObservable()
      .subscribe((change: MediaChange) => {
        // alert(change.mqAlias);
        if (change.mqAlias === 'xs') {
          this.columnNum = 1;
        } else {
          this.columnNum = 2;
        }
      });
   }

  ngOnInit() {
        this._dataApiService
            .getAll()
            .subscribe(
              data => {
                this.foreign_exchange = data;
                console.log(data);
              },
              error => () => {
                console.log('no se conecto.');
            });
  }
  findExchange(id: string): void {
    this._dataApiService
            .getbyId(id)
            .subscribe(
              data => {
                this.rates = data.rates;
                console.log(data.rates);
              },
              error => () => {
                console.log('no se conecto.');
            });
  }
  operationExchange(data1: number, data2: number) {
    this.result = data1 * data2;
  }
}
