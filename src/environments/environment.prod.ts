
// usado cuando se corre 'ng serve --environment prod' o 'ng build --enviroment prod'

export const environment = {
  production: true,

  // URL de la API REST
  apiUrl: 'http://localhost:3000/api'
};
