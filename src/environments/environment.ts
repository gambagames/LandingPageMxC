// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// Usado cuando se corre el comando 'ng serve' o 'ng build'

export const environment = {
  production: true,

  // URL de la API REST
  apiUrl: 'http://localhost:3000/api'
};
